﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;


using MySql;
using MySql.Data.MySqlClient;

using log4net;
using Nini.Config;
using OpenMetaverse;
using Mono.Addins;
using OpenSim.Framework;
using OpenSim.Region.Framework.Interfaces;
using OpenSim.Region.Framework.Scenes;
using OpenSim.Services.Interfaces;
using System.IO;
namespace Pathfinder
{
    /// <summary>
    /// Class used to store the information about a waypoint
    /// sent by the database
    /// </summary>
    public class NodeInfo
    {
        /// <summary>
        /// The region in the simulator where the node is located
        /// Used during the initialization of the module to add
        /// nodes to the graph when a region is loaded in the simulator
        /// </summary>
        private string regionName;

        /// <summary>
        /// Nodes position in the simulator
        /// </summary>
        private Vector3 pos;

        /// <summary>
        /// The neighbours of the node
        /// </summary>
        private List<int> neighbours = new List<int>();

        /// <summary>
        /// The index of the node in the database
        /// </summary>
        private int index;

        /// <summary>
        /// Default constructor that creates a waypoint which is in no region,at the position <0,0,0>
        /// and that has no neighbours
        /// </summary>
        public NodeInfo()
        {
            index = -1;
            regionName = "";
            pos.X = pos.Y = pos.Z = 0;
        }

        /// <summary>
        /// Constructor that creates a waypoint with the given values
        /// </summary>
        /// <param name="index">The index of this node in the graph</param>
        /// <param name="regionName">Name of the region in which the waypoint is located</param>
        /// <param name="pos">Position of the waypoint</param>
        /// <param name="neighbours"></param>
        public NodeInfo(int index,string regionName, Vector3 pos, List<int> neighbours)
        {
            this.index = index;
            this.regionName = regionName;
            this.pos = pos;
            this.neighbours.AddRange(neighbours);
        }

        /// <summary>
        /// Gets or sets the index
        /// </summary>
        public int Index
        {
            get { return index; }
            set { index = value; }
        }

        /// <summary>
        /// Gets or sets the name of the region in which the waypoint is
        /// </summary>
        public string RegionName
        {
            get { return regionName; }
            set { regionName = value; }
        }

        /// <summary>
        /// Gets or sets the position of the node
        /// </summary>
        public Vector3 Pos
        {
            get { return pos; }
            set { pos = value; }
        }

        /// <summary>
        /// Gets the list of the neighbours of this node
        /// </summary>
        public List<int> Neighbours
        {
            get { return neighbours; }
        }


        /// <summary>
        /// Adds a neighbour 
        /// </summary>
        /// <param name="neighbour">neighbour to be added</param>
        public void AddNeighbour(int neighbour)
        {
            neighbours.Add(neighbour);
        }

        /// <summary>
        /// Adds a list of neighbours
        /// </summary>
        /// <param name="neighbours">List of neighbours to be added</param>
        public void AddNeighbour(List<int> neighbours)
        {
            this.neighbours.AddRange(neighbours);
        }



    }

    /// <summary>
    /// The information about a destination
    /// </summary>
    public class WayPoint
    {
        /// <summary>
        /// The destination is just a waypoint with name and description
        /// </summary>
        private string index;
        
        /// <summary>
        /// The namw which will be displayed in the menu in the simulator
        /// </summary>
        private string name;

        /// <summary>
        /// A description which will be displayed in the menu when the user
        /// selects a destination
        /// </summary>
        private string description;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index">Index of the node in the database or graph</param>
        /// <param name="name">Node's name</param>
        /// <param name="description">Node's description</param>
        public WayPoint(string index,string name,string description)
        {
            this.index = index;
            this.name = name;
            this.description = description;
        }

        /// <summary>
        /// Getter and setter for the index 
        /// </summary>
        public string Index
        {
            get { return index; }
            set { index = value; }
        }

        /// <summary>
        /// Getter and setter for the name
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// Getter and setter for the description
        /// </summary>
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
    }

    /// <summary>
    /// Description of a group of destinations
    /// Several destinations share a common place/functionality,
    /// so can be grouped in a region/group
    /// </summary>
    public class RegionInfo
    {
        /// <summary>
        /// Index of the group in the database
        /// </summary>
        private string index;

        /// <summary>
        /// Region's name which will be displayed in the menu
        /// </summary>
        private string name;

        /// <summary>
        /// A description of the region/group of nodes. 
        /// To be displayed in a page containing the description of the region
        /// </summary>
        private string description;

        /// <summary>
        /// Name can be an abbreviation or a representation of the whole name of
        /// a group because of the restrictions imposed by opensim.
        /// full_name contains the whole name of the group. 
        /// The full name of a group is displayed in descriptions page
        /// </summary>
        private string full_name;
        public Dictionary<string,WayPoint> waypoints = new Dictionary<string,WayPoint>();

        public RegionInfo(string index,string name,string description,string full_name)
        {
            this.index = index;
            this.name = name;
            this.description = description;
            this.full_name = full_name;
        }

        /// <summary>
        /// Adds a destination in the region
        /// </summary>
        /// <param name="wp">Destination from the region</param>
        public void AddWayPoint(WayPoint wp)
        {
            waypoints.Add(wp.Name,wp);
        }

        /// <summary>
        /// Getter and setter for index
        /// </summary>
        public string Index
        {
            get { return index; }
            set { index = value; }
        }

        /// <summary>
        /// Getter and setter for name
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// Getter and setter for description
        /// </summary>
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        /// <summary>
        /// Getter and setter for full_name
        /// </summary>
        public string FullName
        {
            get { return full_name; }
            set { full_name = value; }
        }
    }
    /// <summary>
    /// Class that reads the waypoints from a file
    /// </summary>
    public class Tester
    {
        public static Dictionary<string,List<NodeInfo>> ReadFromFile(string FileName)
        {
            Dictionary<string, List<NodeInfo>> nodes = new Dictionary<string, List<NodeInfo>>();

           
            TextReader tr = new StreamReader(FileName);
            NodeInfo ni;
            Vector3 pos;

            string line;
            string [] parameters;
            string [] values;


            while ((line = tr.ReadLine()) != null)
            {
                ni = new NodeInfo();
                parameters = line.Split(new char[]{'|'},StringSplitOptions.None);
                
                ni.Index = int.Parse(parameters[0]);
                ni.RegionName = parameters[1];

                values = parameters[2].Split(new char[] { ' ' }, StringSplitOptions.None);
                pos.X = float.Parse(values[0]);
                pos.Y = float.Parse(values[1]);
                pos.Z = float.Parse(values[2]);
                ni.Pos = pos;

                values = parameters[3].Split(new char[] { ' ' }, StringSplitOptions.None);
                foreach (string value in values)
                {
                    ni.AddNeighbour(int.Parse(value));
                }

                if (!nodes.ContainsKey(ni.RegionName))
                    nodes.Add(ni.RegionName, new List<NodeInfo>() { ni });
                else
                    nodes[ni.RegionName].Add(ni);

            }

            return nodes;
        }

        public static Dictionary<string, List<NodeInfo>> HCNodes()
        {

            Dictionary<string, List<NodeInfo>> nodes = new Dictionary<string, List<NodeInfo>>();

            NodeInfo ni;
        

            ni = new NodeInfo(0, "ACS1", new Vector3(7, 14, 33), new List<int>() { 1 });
            nodes.Add("ACS1",new List<NodeInfo>(){ni});

            ni = new NodeInfo(1, "ACS1", new Vector3(7, 240, 33), new List<int>() { 2 });
            nodes["ACS1"].Add(ni);

            ni = new NodeInfo(2, "ACS1", new Vector3(130, 240, 33), new List<int>() { 3 });
            nodes["ACS1"].Add(ni);

            ni = new NodeInfo(3, "ACS1", new Vector3(130, 14, 33), new List<int>() { 0 });
            nodes["ACS1"].Add(ni);

            return nodes;
            /*
            graph.Add(new Node(0, new Vector3(7, 14, 33), "ACS1"));
            graph[0].AddNeigh(1);

            graph.Add(new Node(1, new Vector3(7, 240, 33), "ACS1"));
            graph[1].AddNeigh(2);

            graph.Add(new Node(2, new Vector3(130, 240, 33), "ACS1"));
            graph[2].AddNeigh(3);

            graph.Add(new Node(3, new Vector3(130, 14, 33), "ACS1"));
            graph[3].AddNeigh(0);
            */

          
        }
    }
    
    /// <summary>
    /// Class which contains methods for sending/retrieving data from the database.
    /// The class provides the following functionalities:
    ///  *  sends the data from a node
    ///  *  sends the neighbours of a node
    ///  *  reads the nodes in a local buffer
    ///  *  clears the local buffer
    ///  *  retrieves the whole graph of nodes
    ///  *  retrieves the nodes from 
    ///  *  retrieves the information about regions and waypoints
    ///  *  gets the number of nodes
    ///  *  connects to the database
    ///  *  disconnects from the database
    /// </summary>
   public class DBInterface
    {
       bool test = false;

       // private string connStr = "server=localhost;user=root;database=opensim;port=3306";

       /// <summary>
       /// Connection with the database
       /// </summary>
       MySqlConnection conn = null;

       /// <summary>
       /// Buffer used to store the information received from the database until
       /// the manager is ready to process them
       /// </summary>
       private Dictionary<string,List<NodeInfo>> buff;

       /// <summary>
       /// Variable which contains the state of the connexion with the database
       /// </summary>
       public bool connected = false;

       /// <summary>
       /// Attempts to connect to the database.
       /// In order to connect to the database the following attributes are required :
       /// host, user, password, database and port.
       /// These attributes are read from the file "pathfinder.cfg" located in the same folder
       /// as the module.
       /// The order is the same as in list above and line from the configuration file must
       /// contain at most one attributes
       /// </summary>
       public DBInterface() 
       {
           
           Connect();
       }

       /// <summary>
       /// Attempts to connect to the database using
       /// the attributes from the configuration file "pathfinder.cfg"
       /// </summary>
       public void Connect()
       {
           //Read the attributes from the configuration file
           TextReader tr = null;
           try
           {
               tr = new StreamReader("pathfinder.cfg");
           }
           catch (Exception ex)
           {
               Manager.m_Logger.Info("Error opening config file");
               Manager.m_Logger.Info("Error: " + ex.Message);
               return;
           }

           string host = tr.ReadLine();
           string user = tr.ReadLine();
           string password = tr.ReadLine();
           string database = tr.ReadLine();
           string port = tr.ReadLine();

           //build the message sent to the database
           string connStr = "server=" + host + ";";
           connStr += "user=" + user + ";";
           if(password != "")
            connStr += "password=" + password + ";";
           connStr += "database=" + database + ";";
           connStr += "port=" + port + ";";
           Manager.m_Logger.Info("[Pathfinder][Database]"+connStr);
           tr.Close();

           //create a connection
           conn = new MySqlConnection(connStr);

         
           try
           {
               //open the connection
               Manager.m_Logger.Info("[Pathfinder][Database]Connecting to MySQL...\n");
               conn.Open();
               connected = true;
               Console.WriteLine("[Pathfinder][Database]Connected MySQL");
           }
           catch (Exception ex)
           {
               Manager.m_Logger.Info(ex.ToString());
           }
       }

       /// <summary>
       /// Closes the connection with the database
       /// </summary>
       public void Disconnect()
       {
         
            conn.Close();
            connected = false;
           Manager.m_Logger.Info("[Pathfinder][Database]Done.");
       }

       /// <summary>
       /// Sends the data from a node
       /// </summary>
       /// <param name="index">Index in the database or graph of nodes</param>
       /// <param name="coords">Node's cartesian coordinates in the simulator</param>
       /// <param name="reg">Index of the group of nodes which contain this node</param>
       /// <param name="regionName">The name of the region from the simulator which contains the node</param>
       /// <returns></returns>
       public bool SendNodeInfo(string index, string[] coords, string reg, string regionName)
       {
           // find out if a node with the same index exists in the database
           // if a node exists then update it's attributes, else insert a new record in the database in the table "nodes"
           // build the query
           string values = index + "," + coords[0] + "," + coords[1] + "," + coords[2] + "," + reg + "," +"\""+ regionName + "\"";
           string sql;

          // string desc = "";
         //  string name = "";
           
        

           string sql_req = "SELECT id,description,name from nodes where id=" + index;
           Manager.m_Logger.Info("sending query: " + sql_req);
         //  Connect();

           try
           {
               //send the query to find out if there exists a node with the same index
               MySqlCommand cmd_req = new MySqlCommand(sql_req, conn);
               MySqlDataReader rdr = cmd_req.ExecuteReader();

               //if a node exists then the result returned by the database contains the row with the attributes of the node
               if (rdr.HasRows)
               {
                   /*
                   if (rdr.Read())
                   {
                       desc = rdr[1].ToString();
                       name = rdr[2].ToString();
                   }
                    */

                  
                   //sql_req = "DELETE FROM nodes WHERE id=" + index;
                   //cmd_req = new MySqlCommand(sql_req, conn);
                 //  cmd_req.ExecuteNonQuery();
                   //build the query for updating the attributes of the node
                   sql = "UPDATE nodes SET x=" + coords[0] + ",y=" + coords[1] + ", z=" + coords[2] + ", reg=" + reg + ", regionName=\"" + regionName + "\" WHERE id=" + index;
               }
               else
               {
                   //no node exists with this index
                   //build the query for inserting a new record
                   sql = "INSERT INTO nodes (id,x,y,z,reg,regionName) VALUES (" + values + ")";
               }

               //close the object used to process the result of a query
               //only one reader must be opened at the same time !!!
               rdr.Close();

               /*
               if (desc == "")
                   sql = "INSERT INTO nodes (id,x,y,z,reg) VALUES ("+values+")";
               else
                   sql = "INSERT INTO nodes (id,x,y,z,reg,description,name) VALUES (" + values + ",'" + desc + "','" + name +"')";
                */
             //  sql="UPDATE nodes SET x="+coords[0]+",y="+coords[1]+", z="+coords[2]+", reg="+reg + " WHERE id="+index;

               //send the query
               Manager.m_Logger.Info("sending query2: " + sql);
               MySqlCommand cmd = new MySqlCommand(sql, conn);
               cmd.CommandText = sql;
               cmd.ExecuteNonQuery();
           }
           catch (Exception ex)
           {
               Manager.m_Logger.Info(ex.ToString());
           }
           
      //     Disconnect();

           return true;
       }    

       /// <summary>
       /// Sends the neighbours of a node to the database
       /// </summary>
       /// <param name="id">Index of the node whose neighbours are sent to the database</param>
       /// <param name="neigh">The neighbours to be sent</param>
       /// <returns></returns>
       public bool SendNodeNeigh(string id,string[] neigh)
       {
       //    Connect();
       
           // query used to insert the neighbours
           string sql_1 = "INSERT INTO neighbours (Index1,Index2) VALUES ";
           string sql;
           
           // query to select the existing neighbours
           string sql_req = "SELECT Index1 from neighbours where ";
           string sql_req_val;

           // find out if the node has neighbouts
           try
           {
               //finalize the query
               sql_req_val = "Index1=" + id;

               //send the query
               MySqlCommand cmd_req = new MySqlCommand(sql_req + sql_req_val, conn);
               MySqlDataReader rdr = cmd_req.ExecuteReader();
               
               //if the node has neighbourss
               if (rdr.HasRows)
               {
                   rdr.Close();
                   //delete the neighbours
                   sql_req = "DELETE FROM neighbours WHERE Index1=" + id;
                   cmd_req = new MySqlCommand(sql_req, conn);
                   cmd_req.ExecuteNonQuery();
               }
               else
                    rdr.Close();

               //send the neighbours to the database
               foreach (string neigh_id in neigh)
               {
                  

                   sql = sql_1 + "(" + id + "," + neigh_id + ")";
                   Manager.m_Logger.Info("[DATABASE] sql : " + sql);
                   MySqlCommand cmd = new MySqlCommand(sql, conn);
                   cmd.ExecuteNonQuery();
               }
           }
           catch (Exception ex)
           {
               Console.WriteLine(ex.ToString());
           }

         //  Disconnect();

           return true;
       }

       /// <summary>
       /// Retrieves the description of the group of destinations from the database
       /// as a dictionary.
       /// The dictionary is used to display menu in the simulator from which
       /// a destination can be chosen or information about a region/destination
       /// can be displayed
       /// 
       /// The dictionary contains as key-value pairs the name of the group
       /// and an object which contains the group of destinations and information about
       /// it
       /// </summary>
       /// <returns>the description of the group of destinations from the database</returns>
       public Dictionary<string,RegionInfo> GetRegionInfo()
       {
           Dictionary<string, RegionInfo> result = new Dictionary<string, RegionInfo>();
           List<string> regions = new List<string>();


           //retrieve the information about the groups of destinations from the database
           try
           {
               
               string sql = "SELECT * from regions";
               MySqlCommand cmd = new MySqlCommand(sql, conn);
               MySqlDataReader rdr = cmd.ExecuteReader();

               while (rdr.Read())
               {
                   string index = rdr[0].ToString();
                   string name = rdr[1].ToString();
                   string description = rdr[2].ToString();
                   string full_name = rdr[3].ToString(); 

                   result.Add(name, new RegionInfo(index, name, description,full_name));


               }
               rdr.Close();
           }
           catch (Exception ex)
           {
               Manager.m_Logger.Info(ex.ToString());
           }

           //retrieve the destinations from each group of nodes from the database
           try
           {
               string sql = "select A.id as id,B.Name as reg,A.description as descr,A.name as name";
                      sql += " from nodes A,(select Id,Name from regions) as B ";
                      sql+=   "where A.reg = B.Id";
               MySqlCommand cmd = new MySqlCommand(sql, conn);
               MySqlDataReader rdr = cmd.ExecuteReader();

               

               while (rdr.Read())
               {
                   Manager.m_Logger.Info((rdr[2].ToString() == "" + "") + "|" + (rdr[3].ToString() == "" + ""));

                   //a destination has the attributes description and name empty
                   //skip the waypoints
                   if ( !(rdr[2].ToString() == "") && !(rdr[3].ToString() == ""))
                   {
                       Manager.m_Logger.Info("here");
                       Manager.m_Logger.Info("region:|" + rdr[1] + "| is in dict:" + (result.ContainsKey(rdr[1].ToString()) + ""));
                       result[rdr[1].ToString()].AddWayPoint(new WayPoint(rdr[0].ToString(),rdr[3].ToString(),rdr[2].ToString()));
                       
                   }
                   
               }
               rdr.Close();

               //for debug only
               foreach (RegionInfo ri in result.Values)
               {
                   Manager.m_Logger.Info( "[Database] Region:"+ ri.Name + " has waypoints :");
                   foreach (WayPoint wp in ri.waypoints.Values)
                   {
                       Manager.m_Logger.Info("[Database]Index " + wp.Index + " NAme: " + wp.Name + " Description:" + wp.Description);
                   }
               }
           }
           catch (Exception ex)
           {
               Manager.m_Logger.Info(ex.ToString());
           }


         

           return result;
       }

         

       /// <summary>
       /// Reads the nodes/waypoints and their neighbours in a buffer 
       /// Note that the destinations are also waypoints
       /// </summary>
       public void ReadNodes()
       {
           if (test)
           {
               buff = Tester.HCNodes();

           }
           else
           {

               string sql = "select regionName region,id,x,y,z ";
               sql += "from nodes;";


               MySqlDataReader rdr = null;
               try
               {
                   buff = new Dictionary<string, List<NodeInfo>>();
                   //read the info about the nodes no neighbors yet
                  MySqlCommand cmd = new MySqlCommand(sql, conn);
                  rdr =  cmd.ExecuteReader();

                   
                  while(rdr.Read())
                  {
                      string region = rdr[0].ToString();

                    //  Manager.m_log.Info("read node from region :" + region+"|");

                      int index = int.Parse(rdr[1].ToString());
                      float x = float.Parse(rdr[2].ToString());
                      float y = float.Parse(rdr[3].ToString());
                      float z = float.Parse(rdr[4].ToString());

                      if(!buff.ContainsKey(region))
                          buff.Add(region,new List<NodeInfo>());

                      buff[region].Add(new NodeInfo(index,region,new Vector3(x,y,z),new List<int>()));

                  }
                  rdr.Close();

                   sql = "SELECT Index2 from neighbours where Index1=";
                   string sql_cmd= "";

                   foreach( List<NodeInfo> lni in buff.Values)
                   {
                       foreach( NodeInfo ni in lni)
                       {
                            sql_cmd = sql + ni.Index;
                            cmd.CommandText = sql_cmd;
                            rdr = cmd.ExecuteReader();
                            while(rdr.Read())
                            {
                                int neigh = int.Parse( rdr[0].ToString() );
                                ni.AddNeighbour(neigh);
                            }
                            rdr.Close();
                       }
                   }
               }
               catch (Exception ex)
               {
                   rdr.Close();
                   Manager.m_Logger.Info("[Database]"+ex.ToString());
               }
           }
       }


       /// <summary>
       /// Gets all the nodes
       /// </summary>
       /// <returns>All the nodes/waypoints from the database</returns>
       public Dictionary<string, List<NodeInfo>> GetNodes()
       {
           return buff;
       }

       /// <summary>
       /// Gets the nodes/waypoints associated with a region from the simulator from the buffer.
       /// The nodes returned will be removed from the buffer
       /// </summary>
       /// <returns>The nodes/waypoints associated with a region from the simulator </returns>
       public List<NodeInfo> GetNodes(string key)
       {
           List<NodeInfo> retVal = null;
           if(buff.ContainsKey(key))
           {
               retVal = buff[key];
               buff.Remove(key);
           }
           return retVal;
       }

       /// <summary>
       /// Get the number of nodes
       /// </summary>
       /// <returns>Number of nodes</returns>
       public int NodesNo()
       {
           int nodes_no = 0;
           int max_index = -1;

           foreach (string key in buff.Keys)
           {
               nodes_no += buff[key].Count;
           }

           foreach (List<NodeInfo> lni in buff.Values)
           {
               foreach(NodeInfo ni in lni)
                max_index = Math.Max(max_index,ni.Index);
           }

           return Math.Max(max_index+1, nodes_no) ;
       }

       /// <summary>
       /// Clears the local buffer
       /// </summary>
       public void Clear()
       {
           buff.Clear();
       }

    }
}
