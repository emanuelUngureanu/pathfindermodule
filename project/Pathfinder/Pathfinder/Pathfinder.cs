﻿///<remarks>
///Class that implements the algorithm required to compute a path
///betwenn the position of the avatar and a point of interest in the world
///</remarks>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

using log4net;
using Nini.Config;
using OpenMetaverse;
using Mono.Addins;
using OpenSim.Framework;
using OpenSim.Region.Framework.Interfaces;
using OpenSim.Region.Framework.Scenes;
using OpenSim.Services.Interfaces;


namespace Pathfinder
{
    /// <summary>
    /// Class that contains the position of a waypoint and it's neighbours
    /// </summary>
    public class Node
    {
        private int index=-1;

        /// <summary>
        /// The position of the waypoint
        /// </summary>
        private Vector3 pos=new Vector3();

        /// <summary>
        /// The neighbours of the waypoint
        /// </summary>
        private List<int> neighbours = new List<int>();

        /// <summary>
        /// The object from the sceen associated with this waypoint
        /// </summary>
        public SceneObjectGroup sog = null;

        /// <summary>
        /// The region where the waypoint is located
        /// </summary>
        private Scene scene = null;

        /// <summary>
        /// A waypoint can be inactive if it is in a region which is not loaded/ was removed from the sim
        /// </summary>
        public bool active = true;

        /// <summary>
        /// Constructs a node with the values conainted in NodeInfo
        /// </summary>
        /// <param name="ni">Node information</param>
        public Node(NodeInfo ni,Scene scene)
        {
            index = ni.Index;
            pos = ni.Pos;
            neighbours = ni.Neighbours;
            this.scene = scene;
        }

        /// <summary>
        /// Constructor for the class Node that receives as parameter the position
        /// of the node in the world
        /// </summary>
        /// <param name="pos">The position of the node</param>
        public Node(int index,Vector3 pos,Scene scene)
        {
            this.index = index;

            this.pos.X = pos.X;
            this.pos.Y = pos.Y;
            this.pos.Z = pos.Z;

            this.scene = scene;
        }

        /// <summary>
        /// Constructor for the class Node that receives as parameter the position
        /// of the node in the world
        /// </summary>
        /// <param name="pos">The position of the node</param>
        public Node(int index,float x, float y, float z)
        {
            this.index = index;
            this.pos.X = x;
            this.pos.Y = y;
            this.pos.Z = z;
        }

        /// <summary>
        /// Adds a neighbour of this node
        /// </summary>
        /// <param name="node">The neighbour of the node</param>
        public void AddNeigh(int node)
        {
            neighbours.Add(node);
        }

        /// <summary>
        /// Adds a list of neighbours of this node
        /// </summary>
        public void AddNeigh(List<int> nodes)
        {
            neighbours.AddRange(nodes);
        }

        public int Index
        {
            get { return index; }
        }

        public Vector3 Pos
        {
            get { return pos; }
        }

        /// <summary>
        /// Returns an array of Nodes that represents the neighbours of this node
        /// </summary>
        /// <returns></returns>
        public int[] Neigh
        {
            get { return neighbours.ToArray(); }
        }

        /// <summary>
        /// Getter for scene
        /// </summary>
        public Scene Scene
        {
            get { return scene; }
        }

        /// <summary>
        /// Getter and setter for active
        /// </summary>
        public bool Active
        {
            get { return active; }
            set { active = value; }
        }
    }


    /// <summary>
    /// Class that computes paths between waypoints and that contains
    /// the paths computed until the user that requested a path to a destination
    /// has reached it.
    /// </summary>
    class Pathfinder
    {
        /// <summary>
        /// All the path computed for which the destination has not been reached
        /// </summary>
        public Dictionary<UUID,List<int>> paths = new Dictionary<UUID,List<int>>();

        /// <summary>
        /// graph of waypoints
        /// </summary>
        public List<Node> graph;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="graph">Graph of waypoints required to compute a path</param>
        public Pathfinder(List<Node> graph)
        {
            this.graph = graph;
        }

        /// <summary>
        /// Computes a path between a start waypoint and a destination waypoint.
        /// The path will be used to allow an avatar with the UUID id to navigate
        /// to the destination
        /// The path will be saved in the dictionary paths for later use
        /// 
        /// The algorithm used to compute the path is the standard A* algorithm
        /// </summary>
        /// <param name="id">The ID of the avatar that requested a path</param>
        /// <param name="start">The index of the start point</param>
        /// <param name="stop">The index of the destination</param>
        /// <returns>The path generated</returns>
        public List<int> GetPath(UUID id,int start,int stop)
        {
            //Manager.m_Logger.Info(" pathf" + "getpath: "+ graph==null+"\nid:"+id);

            paths.Remove(id); 

            List<int> closedSet = new List<int>();
            List<int> openSet = new List<int>();
           
            int[] came_from = new int[graph.Count];
            float[] f_score = new float[graph.Count];
            float[] g_score = new float[graph.Count];

           
         
            for (int i = 0; i < graph.Count; i++)
                came_from[i] = -1;
                        
            //for testing purposes there is a limit on the number of loops 
            int count = 100;
            int current;
            float tentative_g_score;

            openSet.Add(start);

            g_score[start] = 0;
            f_score[start] = g_score[start] + Cost_estimate(start, stop);

            Manager.m_Logger.Info("pathf" + "before main loop");

            while (openSet.Count > 0 && count-- > 0)
            {
                current = GetLowest(openSet, f_score);

                string message = "";
                foreach (int z in openSet) message += z + " ";

                Manager.m_Logger.Info("pathf" + "current node is:" + current + " " + message);
                if (current == stop)
                {
                    Manager.m_Logger.Info("pathf" + "reached dest");
                    return ReconstructPath(id,came_from,current);
                }

                openSet.Remove(current);
                closedSet.Add(current);


                foreach (int neigh_index in graph[current].Neigh)
                {
                    Node neigh = graph[neigh_index];
                    if(closedSet.Contains(neigh.Index))
                        continue;

                    tentative_g_score = g_score[current] + Cost_estimate(current,neigh.Index);

                    if( !openSet.Contains(neigh.Index) || tentative_g_score < g_score[neigh.Index])
                    {
                        openSet.Add(neigh.Index);
                        came_from[neigh.Index] = current;
                        g_score[neigh.Index] = tentative_g_score;
                        f_score[neigh.Index] = g_score[neigh.Index] + Cost_estimate(neigh.Index,stop);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Computes the cost between two points using the Euclidean metric
        /// </summary>
        /// <param name="first">Index of the first point</param>
        /// <param name="second">Index of the second point</param>
        /// <returns></returns>
        private float Cost_estimate(int first, int second)
        {
            return Vector3.Distance(graph[second].Pos, graph[first].Pos);
        }

        /// <summary>
        /// Finds the index of the node with the lowest f_score
        /// </summary>
        /// <param name="openSet">The openset used in the A* algorithm</param>
        /// <param name="f_score">The score each node</param>
        /// <returns>index of the node with the lowest f_score</returns>
        private int GetLowest(List<int> openSet, float[] f_score)
        {
            int pos = openSet[0];

            foreach (int i in openSet)
                if (f_score[i] < f_score[pos])
                    pos = i;

            return pos;
        }

        /// <summary>
        /// Function used to build the path found by the algorithm
        /// and add it to the dictionary for later use
        /// </summary>
        /// <param name="id">ID of the avatar that requested a path</param>
        /// <param name="came_from">Array that that contains the "parent" of each node from the path</param>
        /// <param name="current">The destination of the path</param>
        /// <returns></returns>
        private List<int> ReconstructPath(UUID id,int[] came_from,int current)
        {
            List<int> path = new List<int>();

            while (came_from[current] != -1)
            {
                path.Insert(0, current);
                current = came_from[current];
            }

            path.Insert(0, current);

           // Manager.mut.WaitOne();
            paths.Add(id, path);
           // Manager.mut.ReleaseMutex();

            return path;
        }

        /// <summary>
        /// Get the waypoint which the avatar must reach
        /// </summary>
        /// <param name="id">The id of the avatar</param>
        /// <returns>The waypoint which the avatar must reach</returns>
        public Node GetCurrentWaypoint(UUID id)
        {
            //Manager.mut.WaitOne();
            Node node = null;

            if (paths.ContainsKey(id))
            {
                node = graph[ ( (paths[id])[0]) ];
            }

           // Manager.mut.ReleaseMutex();

            return node;
        }

        /// <summary>
        /// Get the next waypoint which the avatar must reach
        /// </summary>
        /// <param name="id">Avatar id</param>
        /// <param name="index"></param>
        /// <returns></returns>
        public Node GetNextWaypoint(UUID id,int index)
        {
           // Manager.mut.WaitOne();

            if (!paths.ContainsKey(id))
                return null;
           
            //get the path followed by the avatar with the UUID id
            //note that this list is a copy of the list from paths
            //so if the path is changed by removing the first waypoint
            //the path must be removed from paths and then inserted again
            //performance killer
            List<int> path = paths[id];
            
            //remove the current waypoint ( the first waypoint from the path)
            path.Remove(path[0]);

            //remove the path from paths
            paths.Remove(id);


            //if there is another waypoint in the path , add the path to the list
            //of current paths and assign the first waypoint found to the result wp.
            Node wp = null;
            if (path.Count != 0)
            {
                paths.Add(id, path);
                wp = graph[path[0]];
                
              
            }

           // Manager.mut.ReleaseMutex();

            return wp;

        }

        /// <summary>
        /// Clears the graph of nodes
        /// </summary>
        public void Clear()
        {
            graph.Clear();
        }
    }
}
