﻿///<remarks>
///  2012,Ungureanu Emanuel
///  Pathfinder Module
///  
///  This module implements pathfinding and orientation functionalities.
///  It allows a user to find important places and the path to them.
///  
///  The module uses the A* algorith to find a path from the avatars location
///  to the selected destination
///  
///  The data is stored in an MySQL database
/// 
///  For more info check the README
///</remarks>

using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

using MySql;
using MySql.Data.MySqlClient;

using log4net;
using Nini.Config;
using OpenMetaverse;
using Mono.Addins;
using OpenSim.Framework;
using OpenSim.Region.Framework.Interfaces;
using OpenSim.Region.Framework.Scenes;
using OpenSim.Services.Interfaces;
using System.Threading;


[assembly: Addin("MoveAvModule", "0.1")]
[assembly: AddinDependency("OpenSim", "0.5")]


namespace Pathfinder
{
    [Extension(Path = "/OpenSim/RegionModules", NodeName = "RegionModule")]
    public class Manager : ISharedRegionModule
    {
        #region variables

        /// <summary>
        /// Each message written to the console/log file must be prefixed with this string in
        /// order to identify the messages from this module
        /// </summary>
        protected readonly string m_LogMessagePrefix = "[Pathfinder]";
        
        /// <summary>
        /// Logger
        /// </summary>
        protected internal static readonly ILog m_Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);


        protected internal static Mutex mut = new Mutex();
        
        
        /// <summary>
        /// Dictionary which contains as key-value pairs the name of a region and the regions interface for
        /// script communication. The interface is used by the module to send and receive messages from scripts
        /// </summary>
        private Dictionary<String, IScriptModuleComms> m_dictCommInterface = new Dictionary<string, IScriptModuleComms>();


        /// <summary>
        /// Dictionary which contains as key-value pairs the name of a region and a reference to the region/scene.
        /// There are several informations needed from the scene object, such as the name of the region and the interface with
        /// the scripts from the region. So it is required to keep a reference to all the regions which contain waypoints or
        /// important places
        /// </summary>
        private Dictionary<String,Scene> m_dictScenes = new Dictionary<String,Scene>();

        /// <summary>
        /// Dictionary which contains as key-value pairs the name of a region and the menu entry associated with the region
        /// which will be displayed to the user.
        /// </summary>
        private Dictionary<string, RegionInfo> m_dictRegionInfo = new Dictionary<string, RegionInfo>();

        /// <summary>
        /// the interface which allows this module acces to the programms console
        /// </summary>
        private ICommandConsole m_ConsoleInterface = null;

        /// <summary>
        /// Class that computes a path between two nodes in the graph and stores the paths
        /// requested by the avatars
        /// </summary>
        private Pathfinder m_Pathfinder;

        /// <summary>
        /// Graph of waypoints
        /// </summary>
        private List<Node> m_WaypointGraph = null;
        
        /// <summary>
        /// Interface used to communicate with the database
        /// </summary>
        private DBInterface m_DbInterface;
                       
        /// <summary>
        ///  Variable used to store the state of this module
        ///  The module enters the running state after Initialise is called and
        ///  it enters the "not running" state after Close is called
        /// </summary>
        private bool m_Running = false;
        #endregion

        #region IRegionModuleBase implementation

        public void PostInitialise() { }
        
        /// <summary>
        /// Called at server startup
        /// Initialises the interface with the database, fetches the menu 
        /// displayed to the user, and reads the graph in a buffer.
        /// </summary>
        /// <param name="config"></param>
        public void Initialise(IConfigSource config)
        {
            //the module is loaded in OpenSim 
            m_Running = true;

            m_Logger.Info(m_LogMessagePrefix + "Initializing");


            //initialise the interface with the database
            m_DbInterface = new DBInterface();

            //initialise the class which computes paths and and shows an avatar the next waypoint from a path
            m_Pathfinder = new Pathfinder(m_WaypointGraph);

            //tell the interface with the database to read the waypoints and store them in a buffer
            if (m_DbInterface.connected)
            {
                m_dictRegionInfo = m_DbInterface.GetRegionInfo();
                m_DbInterface.ReadNodes();
            }

            //get the inferface which allows acces to the console
            m_ConsoleInterface = MainConsole.Instance;

            //register a command so that commands can be sent to this module from the console
            m_ConsoleInterface.Commands.AddCommand("PathfinderModule", false, "pathf", "pathfinder module", "pathfinder module", ProcessConsoleCommand);

            
        }


        /// <summary>
        /// Called when the module is unloaded 
        /// The console command module unload calls this function but the module remains loaded in the simulator
        /// </summary>
        public void Close()
        {
            m_Logger.Info(m_LogMessagePrefix + "Closing");

            //clear the dictionaries
            m_dictCommInterface.Clear();
            m_dictScenes.Clear();
            m_WaypointGraph.Clear();

            //clear local data from the interfaces and remove them
            m_Pathfinder.Clear();
            m_Pathfinder = null;

            m_DbInterface.Clear();
            m_DbInterface = null;

            m_ConsoleInterface = null;

            //mark the module as not running
            m_Running = false;

        }

        public string Name
        {
            get { return "PathfinderModule"; }
        }

        public Type ReplaceableInterface
        {
            get { return null; }
        }

        /// <summary>
        /// Called when a region is added in the simulator
        /// </summary>
        /// <param name="scene">The scene that is added to the world.The scene added is not initialised</param> 
        public void AddRegion(Scene scene)
        {
          m_Logger.Info(m_LogMessagePrefix + "Region " + scene.RegionInfo.RegionName + "added");

          //When OpenSim is not in the startup state this method is called when a region is added
          //If the region has been removed before the nodes from that region have been marked as inactive
          //and they must be reactivated
          if(m_WaypointGraph != null)
              foreach (Node node in m_WaypointGraph)
              {
                  //when this module initialises it fill the list of nodes with null
                  if (node == null)
                      continue;

                  if (node.Scene == scene)
                      node.Active = true;
              }
        }

        /// <summary>
        /// Called when a region is removed from the world
        ///  scene   the region that will be removed
        /// </summary>
        /// <param name="scene">The region that will be removed</param>
        public void RemoveRegion(Scene scene)
        {
            m_Logger.Info(m_LogMessagePrefix + "Region " + scene.RegionInfo.RegionName + "removed");

            // All the nodes that are located in the removed region have to be marked as inactive
            foreach(Node node in m_WaypointGraph)
            {
                if (node.Scene == scene)
                    node.Active = false;
            }

            //remove the ScriptCommsModule associated with the region that will be removed
            m_dictCommInterface.Remove(scene.RegionInfo.RegionName);
            m_dictScenes.Remove(scene.RegionInfo.RegionName);

            if (m_dictScenes.Count == 0)
                m_DbInterface.Disconnect();
            
        }

        /// <summary>
        /// Called when a region is loaded in the simulator ( the region is iniatilised )
        /// </summary>
        /// <param name="scene">The region loaded</param>
        public void RegionLoaded(Scene scene)
        {
            m_Logger.Info(m_LogMessagePrefix + "Region " + scene.RegionInfo.RegionName + "loaded");
         
            //request the script communication module associated with the region
            m_dictCommInterface.Add(scene.RegionInfo.RegionName,scene.RequestModuleInterface<IScriptModuleComms>());
        
            //register the function that will be called when a script from the world send a message to a module
            m_dictCommInterface[scene.RegionInfo.RegionName].OnScriptCommand += ProcessScriptCommand;

            //add the scene to the list of scenes this module works with
            m_dictScenes.Add(scene.RegionInfo.RegionName,scene);

            if (m_DbInterface.connected)
            {
                //the first time RegionLoaded is called the graph must be initialized 
                if (m_WaypointGraph == null)
                {
                    //get the size of the graph/number of nodes
                    int size = m_DbInterface.NodesNo();
                    m_WaypointGraph = new List<Node>();
                    //fill the graph with null values
                    //the graph is initialized with null values because the nodes from a region are added when the region
                    //is loaded in the simulator and order in which regions are added is not the same with the order
                    //they are stored
                    for (int i = 0; i < size; i++)
                    {
                        m_WaypointGraph.Add(null);
                    }
                }

                //set the nodes from this region in the graph
                AddNodes(m_DbInterface.GetNodes(scene.RegionInfo.RegionName), scene);

                //if there graph is not empty update the graph from the instance which calculates the path
                if (m_DbInterface.NodesNo() == 0)
                    m_Pathfinder.graph = m_WaypointGraph;
            }

        }
        
        #endregion

       
        /// <summary>
        /// Copies the nodes from the list lni into given position in the graph
        /// This method does not add nodes, it just sets copies in the graph/list
        /// </summary>
        /// <param name="lni">List of nodes to be copied</param>
        /// <param name="scene">The scene which contains the nodes</param>
        public void AddNodes(List<NodeInfo> lni,Scene scene)
        {
            //if there are no nodes in the list return
            if (lni == null)
                return;
            
            //go through the list and set each node in the graph
            foreach (NodeInfo ni in lni)
            {
              //copy the node at the position "index"
              m_WaypointGraph[ni.Index] = new Node(ni, scene);
              m_Logger.Info(m_LogMessagePrefix + "Scene :" + ni.RegionName + " Added node : " + ni.Index + " " + ni.Pos + " " + "With :" + ni.Neighbours.Count + " neighbours ");
            }
        }

        /// <summary>
        /// Updates the graph of nodes
        /// </summary>
        /// <param name="niDict"></param>
        public void UpdateNodes()
        {
            //TODO check connection with the database

            //read nodes in a buffer
            m_DbInterface.ReadNodes();
            
            //get the new nodes
            Dictionary<string, List<NodeInfo>> niDict = m_DbInterface.GetNodes();

            //clear the buffer
            m_DbInterface.Clear();

            //delete the old graph
            m_WaypointGraph.Clear();

            
            //BUG after the graph is cleared , it should be filled
            //with empty nodes because the methot AddNodes sets the nodes
            //at given positions and does not add them to the graph/list
            //TODO fixbug in the method ProcessConsoleCommand
            // the instructions which deal with the command "pathf db reload" 
            // also reload the data from the database and there the graph is
            // filled with empty nodes

            //build the new graph
            foreach (string key in niDict.Keys)
            {
                AddNodes(niDict[key], m_dictScenes[key]);
            }

            //update the graph in the pathfinder instance
            m_Pathfinder.graph = m_WaypointGraph;


        }

        /// <summary>
        /// Called when a script sends a command to a module
        /// The commands which can be received are:
        ///   1. mesage =  data | RegionName | NodeName | X | Y | Z | RegionId Neighbours  sent by Node.lsl
        ///   2. message = "", k = "reload" sent by Commander.lsl
        ///   3. commands from Pannel.lsl see ProcessPannelCommand for more info
        /// </summary>
        /// <param name="scriptID">The id of the sending script</param>
        /// <param name="reqID">The id of the request</param>
        /// <param name="module">The name of the destination module</param>
        /// <param name="message">The message sent by the script</param>
        /// <param name="k">The key sent by the script</param>
        public void ProcessScriptCommand(UUID scriptID, string reqID, string module, string message, string k)
        {
            //if the module has been unloaded( Close() has been called) return;
            if (!m_Running)
                return;

            //if the message is not for this module return;
            if (module != "PathfinderModule")
                return;


            // A Commander script sent the command reload
            // This command tells the module to reload the data from the database
            if ( message.Length == 0 && k == "reload")
            {
                /*
                 * if the same person choses the menu entry "Reload data" from the Commander script
                 * two times in a row it may happend that the commands are sent to the module
                 * at the same time. The method ProcessConsoleCommands uses a mutex to serialize
                 * the acces to the database
                 */
               // mut.WaitOne();
                ProcessConsoleCommand("PathfinderModule", new string[] { "pathf","db","reload" });
               // mut.ReleaseMutex();
                return;
            }

            if (k.Length != 0)
            {
                m_Logger.Info("received message from a pannel");
                ProcessPannelCommand(message, k,scriptID);
                return;
            }

           
            m_Logger.Info(m_LogMessagePrefix+"Received message from a script:"+message);

            //get the parameters of the message
            string[] tokens = message.Split(new char[] { '|' }, StringSplitOptions.None);

            //the message is from a waypoint
            //an avatar has reached a waypoint. The module checks if the avatar follows a path
            //and activates the next point in the path if necessary.
            if( tokens[0] == "waypoint" )
            {
                ProcessWayPointCommand(tokens);
            }
            //TODO useless if 
            else if (tokens[0] == "request" )
            {
                m_Logger.Info(m_LogMessagePrefix + "An avatar requested a path :" + message);
                //ProcessPannelCommand(message);
            }
            //a node wants to send data to the database
            else if (tokens[0] == "data")
            {
                m_Logger.Info(m_LogMessagePrefix + "sending data to db :" + message);
                
                //remove the name of the region and send it as parameter to the method
                List<string> paramms = new List<string>();
                paramms.Add(tokens[0]);
                for (int i = 2; i < tokens.Length; i++)
                    paramms.Add(tokens[i]);

                //the method communicates with the database
                //all the communications with the database within this module must be serial
              //  mut.WaitOne();
                SendNodeInfo(paramms.ToArray(),tokens[1]);
               // mut.ReleaseMutex();
                /*
                foreach (Scene scene in m_scenes.Values)
                {
                    EntityBase[] ebar = scene.GetEntities();
                    foreach (EntityBase eb in ebar)
                    {
                        SceneObjectPart sop = scene.GetSceneObjectPart(eb.UUID);
                        List<UUID> ltii = sop.Inventory.GetInventoryList();
                        foreach (UUID id in ltii)
                        {
                            if (id == scriptID)
                            {
                                mut.WaitOne();
                                SendNodeInfo(tokens, scene.RegionInfo.RegionName);
                                mut.ReleaseMutex();
                                return;
                            }
                        }
                    }
                }*/

                
            }
            else{
                //TEST show the value of the avatars ID on the console
                m_Logger.Info(m_LogMessagePrefix + message);

                ScenePresence avatar;
                foreach (Scene scene in m_dictScenes.Values)
                {
                    
                    foreach (EntityBase eb in scene.GetEntities())
                    {
                        if (!scene.TryGetAvatarByName(eb.Name,out avatar))
                            continue;
                        m_Logger.Info(m_LogMessagePrefix + "Avatar LocalID:" + avatar.LocalId);
                        m_Logger.Info(m_LogMessagePrefix + "Avatar UUID:" + avatar.UUID);

                    }
                }
           }
        }

        /// <summary>
        /// The attributes of a waypoint are sent by a script as a string
        /// This method processes the string and extracts the attributes of the waypoint
        /// and then sends them to the database.
        /// </summary>
        /// <param name="tokens">The attributes of a waypoint</param>
        /// <param name="region">The region/zone where the waypoint is</param>
        public void SendNodeInfo(string[] tokens,string region)
        {
            //TODO here and evertwhere else there are operations with the database:
            //don't maintain an open connection with the database, open it when the program
            //needs to communicate with the DB and close it after it's done

            //check if there is a connection with the database

            if (tokens.Length < 6)
            {
                m_Logger.Info(m_LogMessagePrefix + "SendNodeInfo: Invalid number of parameters");
                return;
            }

            if (!m_DbInterface.connected)
            {
                m_Logger.Info(m_LogMessagePrefix + "SendNodeInfo:No connection with database");
                return;
            }
            /*
             *                                                 0           1              2
             19:40:09 - [Pathfinder]:[Log]sending data to db :data|43.7029113769531|147.863739013672|33.8567695617676|ACS1|1 3
             19:40:09 - [Pathfinder]:[Console]Region 1 3 not found
             */
            //a waypoint sends data to the database
            //the data send  contains : id,coordinates,region,description(optional) and neighbours

            //if the region is not present in the database
            //do not add the waypoint
            
           
            //get the index of the node
            string index = tokens[1];

            //the index is sent as a string to the database but it should be an integer represented as string
            //check if it integer to avoid errors in sending data to the DB
            int indecs;
            try
            {
                indecs = int.Parse(tokens[1]);
            }
            catch (System.ArgumentNullException e)
            { m_Logger.Info(m_LogMessagePrefix + "Invalid  index.Error: " + e.Message); return; }
            catch (System.FormatException e)
            { m_Logger.Info(m_LogMessagePrefix + "Invalid  index.Error: " + e.Message); return; }
            catch (System.OverflowException e)
            { m_Logger.Info(m_LogMessagePrefix + "Invalid  index.Error: " + e.Message); return; }


            //parse the coordinates
            string[] coords = new string[] { tokens[2], tokens[3], tokens[4] };

            //get the second part of the attributes, it's the second part because it uses 
            //a different separator , a space " "
            string[] tokens2 = tokens[5].Split(new char[] { ' ' }, StringSplitOptions.None);

            //check if there is a region with the given name
            if (tokens2.Length == 0 && !m_dictRegionInfo.ContainsKey(tokens2[0]))
            {
                m_Logger.Info(m_LogMessagePrefix + "Region \"" + tokens[5] + "\" not found ");
                return;
            }

            //get the index of the region where this node is situated
                       

            string reg = m_dictRegionInfo[tokens2[0]].Index;

        

            //send the information of the node to the database
            m_DbInterface.SendNodeInfo(index, coords, reg,region);


            
            
            //parse the neighbours
            if (tokens2.Length > 1)
            {
                m_Logger.Info(m_LogMessagePrefix + "Neigh: " + tokens[5]);
                string[] neigh = new string[tokens2.Length - 1];
                for (int i = 1; i < tokens2.Length; i++)
                {
                    m_Logger.Info(tokens2[i] + "|||||||");
                    neigh[i - 1] = tokens2[i];
                }

                //send the neibhbours to the database
                m_DbInterface.SendNodeNeigh(index, neigh);


            }


/*
            dbIface.SendNodeInfo(index, coords, region);
            
            if (tokens[6].Length > 0)
            {
                m_log.Info(console_tag + "Neigh: " + tokens[6]);
                string[] neigh = tokens[6].Split(new char[] { ' ' }, StringSplitOptions.None);
                foreach (string n in neigh)
                    m_log.Info(n + "|||||||");
                dbIface.SendNodeNeigh(index, neigh);
              
                
            }
 * */
             

        }

        /// <summary>
         ///an avatar has reached a waypoint. The module checks if the avatar follows a path
        ///and activates the next point in the path if necessary.
        /// </summary>
        /// <param name="tokens">The message sent by the wapyoint</param>
        public void ProcessWayPointCommand(string[] tokens)
        {
            //if there is no graph return
            if (m_WaypointGraph == null)
            {
                m_Logger.Info(m_LogMessagePrefix + "ProcessWayPointCommand:No graph");
                return;
            }

            m_Logger.Info(m_LogMessagePrefix + " An avatar has reached a waypoint");


            ScenePresence avatar;
            int nodeIndex;
            string regionName = tokens[2];
            string avatarName = tokens[3];

            //get the index of the waypoint
            try
            {
                nodeIndex = int.Parse(tokens[1]);
            }
            catch (System.ArgumentNullException e)
            { m_Logger.Info(m_LogMessagePrefix + "Invalid  index.Error: " + e.Message); return; }
            catch (System.FormatException e)
            { m_Logger.Info(m_LogMessagePrefix + "Invalid  index.Error: " + e.Message); return; }
            catch (System.OverflowException e)
            { m_Logger.Info(m_LogMessagePrefix + "Invalid  index.Error: " + e.Message); return; }

            //try to get the ScenePresence for the avatar that reached the waypoint
            //if the ScenePresence could not be retrieved return
            if (!m_dictScenes[regionName].TryGetAvatarByName(avatarName, out avatar))
                return;

            //false avatar => return;
            if (avatar == null)
            {
                m_Logger.Info(m_LogMessagePrefix + "Couldnt find any avatar with the name " + avatarName + " in the region " + regionName);
                return;
            }
            m_Logger.Info(m_LogMessagePrefix + "Waypoint reached: " + nodeIndex);

            UUID id = avatar.UUID;

            //check if there is another waypoint in the current path of the avatar    
            Node next_node = m_Pathfinder.GetCurrentWaypoint(id);

            //TODO this "if" should be moved after the try catch block
            if (next_node == null)
                return;

            //check if the waypoint reached by the avatar is the same as the waypoint from the path
            bool next = next_node.Index == nodeIndex;
            m_Logger.Info(m_LogMessagePrefix + "Correct waypoint reached");
            m_Logger.Info(m_LogMessagePrefix + "dest:" + next);

            //if the avatar reached the correct waypoint 
            if (next)
            {
                //get the next waypoint
                Node dest = m_Pathfinder.GetNextWaypoint(avatar.UUID, nodeIndex);

                //if there are no more waypoints return
                if (dest == null)
                    return;

                m_Logger.Info(m_LogMessagePrefix + "Pos:" + dest.Pos.X + " " + dest.Pos.Y + " " + dest.Pos.Z);
                //send the client used by the user the command to display an arrow pointing to the next waypoint
                avatar.ControllingClient.SendScriptTeleportRequest("primitive", dest.Scene.RegionInfo.RegionName, dest.Pos, new Vector3(0, 0, 0));
                List<int> path = m_Pathfinder.paths[avatar.UUID];
                string msg = "";
                foreach (int z in path) msg += z + " ";
                m_Logger.Info(m_LogMessagePrefix + "path:" + msg);
            }
        }

        /// <summary>
        /// This method processes commands sent by the pannel
        /// 
        /// When the menu is firstly displayed it will display a list of zones of possible destinations. Because there are many zones
        /// zones are organized in pages , so the menu also contains the buttons "Page:[previos page index]" and "Page:[next page index]"
        ///  If the user selects anoter page oter zones will be displayed.
        ///  
        ///  When the user selects a region the menu will contain a list of possible destinations , the buttons for pages ,the button 
        ///  "Back to regions" and the one with the tag "More info", because the destination are organized in pages.
        ///  When a user pushes a button from the menu the Pannel.lsl script sends only the tag of the button and information used to identify the 
        ///  script and the avatar. In order to identfy the zone which contains the destination without searching it in the data the tag 
        ///  of a destination button is :
        ///  [ZoneName]:[Destination]. 
        ///  
        ///  The tag of a region is [ZoneName].
        ///  
        ///  When the user selects a destinatin the menu will display a description of the destination and the buttons [ZoneName]:[Destination]:[Select] and 
        ///  "Back to [ZoneName]". 
        ///   
        ///  
        /// 
        /// </summary>
        /// <param name="message">Option from the menu selected by the avatar</param>
        /// <param name="k">Information required to identify the script which sent the command and the avatar</param>
        /// <param name="scriptID"></param>
        public void ProcessPannelCommand(string message,string k,UUID scriptID)
        {
            //if there is no data 
            if (m_dictRegionInfo == null)
            {
                m_Logger.Info("ProcessPannelCommand: No region info available");
                return;
            }

            // the avatar selected the button "Back to regions"
            if (message == "Back to regions")
            {
                {
                    int page = 1;
                    string[] buttons = region_buttons(page);
                    string msg = "Pentru descrierea prescurtarilor apasati butonul \"More info\" de pe prima pagina\n";
                    msg += "Zone disponibile :";
                    SendDialogToUser(msg, buttons, k);

                    return;
                }
            }
            //the avatar selected the button "Back to [ZoneName]".
            else if (message.StartsWith("Back to "))
            {
                string region = message.Substring("Back to ".Length);
                string[] buttons = destination_buttons(1, region);
                string msg = "Destinatii posibile in zona " + region + ":";
                SendDialogToUser(msg, buttons, k);

                return;
            }
            //the avatar selected the button "More info"
            if (message == "More info")
            {
                string[] buttons = {"Back to regions"};

                //if the avatar selected more info, display what the shortcuts mean
                //TODO Organize the description of shortcuts in pages
                string description = shortcuts();

                SendDialogToUser(description, buttons, k);

                return;
            }

            string[] tokens = message.Split(new char[] { ':' }, StringSplitOptions.None);
            
            // the avatar selected the button [ZoneName]
            if (tokens.Length == 1 )
            {
                //a fost aleasa o regiune
                string[] buttons = destination_buttons(1, tokens[0]);
                string msg = "Pentru descrierea prescurtarilor apasati butonul \"More info\" de pe prima pagina\n";
                msg += "Destinatii posibile in zona " +tokens[0] +":";
                SendDialogToUser(msg, buttons, k);
            }
            //the avatar selected the button "Page:[PageNumber]" or "[ZoneName]:[Destination]"
            else if (tokens.Length == 2 )
            {
                //a fost o pagina cu regiuni sau a fost aleasa o destinatie

                //the avatar selected the button "Page:[PageNumber]"
                if (tokens[0] == "PAGE")
                {
                    int page = int.Parse(tokens[1]);
                    string[] buttons = region_buttons(page);
                    string msg = "Zone disponibile :";
                    SendDialogToUser(msg, buttons, k);
                }
                //the avatar selected the button "[ZoneName]:[Destination]"
                else
                {
                    string[] buttons = new string[] { tokens[0] + ":" + tokens[1] + ":" + "select", "Back to "+tokens[0] };
                    

                    string msg = m_dictRegionInfo[tokens[0]].waypoints[tokens[1]].Description;
                    SendDialogToUser(msg, buttons, k);
                }

            }
            //the avatar selected one of the buttons: "[ZoneName]:[Page]:[PageNumber]", "[ZoneName]:[Destination]:select"
            else if (tokens.Length == 3)
            {
                //the avatar selected "[ZoneName]:[Page]:[PageNumber]", 
                if (tokens[1] == "PAGE")
                {
                    //a fost aleasa o pagina cu destinatii dintr-o regiune
                    //nu a fost aleasa o pagina de regiuni
                    int page = int.Parse(tokens[2]);
                    string[] buttons = destination_buttons(page, tokens[0]);
                    string msg = "Destinatii posibile in zona " + tokens[0] + ":";
                    SendDialogToUser(msg, buttons, k);
                }
                else
                {
                    //the avatar selected "[ZoneName]:[Destination]:select"
                    
                    //get the information about the avatar and the sending script
                    string[] paramms = k.Split(new char[] { '|' }, StringSplitOptions.None);
                    string msg = "The avatar selected a destination " + "||||";
                    foreach (string s in paramms)
                        msg += s + " ";
                    msg += "||||";
                    foreach (string s in tokens)
                        msg += s + " ";
                    m_Logger.Info(m_LogMessagePrefix + msg);
                    
                    ScenePresence sp;
                    //get the name of the avatar;
                    m_dictScenes[paramms[0]].TryGetAvatarByName(paramms[2], out sp);

                    //get the UUID of the avatar
                    UUID id = sp.UUID;

                    //if the avatar requested a path before, delete it
                    m_Pathfinder.paths.Remove(id);

                    //get the starting point
                    int start = int.Parse(paramms[3]);

                    //get the destination point
                    int stop =int.Parse( m_dictRegionInfo[tokens[0]].waypoints[tokens[1]].Index);

                    //get the path from start to stop
                    m_Logger.Info(m_LogMessagePrefix + "Computing path to destination start:stop = " + start + ":" + stop);
                    List<int> path = m_Pathfinder.GetPath(id, start, stop);
                    
                    msg = "Path:";
                    foreach(int i in path)
                        msg += i + " ";
                    m_Logger.Info(m_LogMessagePrefix + msg);
                    //get the id of object which contains the script Pannel.lsl which sent the message/command
                    UUID objID = new UUID();
                    UUID.TryParse(paramms[1], out objID);
                
                    //get the SceneObjectGroup which conatins the object
                    SceneObjectGroup sog = m_dictScenes[paramms[0]].GetSceneObjectGroup(objID);


                    //send a coomand to the client used by the user to display an arrow pointing to the next waypoint
                    sp.ControllingClient.SendScriptTeleportRequest(sog.Name, paramms[0], m_Pathfinder.GetCurrentWaypoint(sp.UUID).Pos, new Vector3(0, 0, 0));

                    //get the destination for logging purpose
                    Node dest = m_Pathfinder.GetCurrentWaypoint(sp.UUID);

                    m_Logger.Info(m_LogMessagePrefix + "Pos:" + dest.Pos.X + " " + dest.Pos.Y + " " + dest.Pos.Z);

                }
                
            }

         

            return;
            }
        

      /// <summary>
      /// Builds a string containing a descriptoin of the shortcuts of the names of the zones
      /// </summary>
      /// <returns>A string containing the description of the shortcuts of the names of the zones</returns>
        public string shortcuts()
        {
            string button = "Prescurtarile folosite in prezentarea zonelor de interes sunt:\n";

            //get the name of regions
            string [] keys = m_dictRegionInfo.Keys.ToArray();

            //if there are registered regions
            if(keys.Length > 1)
                //for each region
                for (int i = 0; i < keys.Length - 1; i++)
                {
                    //add the text [regionShortcut] = [regionFullName] , for example ACS= Automatica si Calculatoare
                    button += keys[i] + "=" + m_dictRegionInfo[keys[i]].FullName + "\n";
                }

            if (button.Length > 2)
                button += keys[keys.Length - 1] + "=" + m_dictRegionInfo[keys[keys.Length - 1]].FullName;

            
            return button;
        }
        
        /// <summary>
        /// Builds the menu containing the destinations from a zone/region
        /// More info in ProcessPannelCommand
        /// </summary>
        /// <param name="page">Page of destinations to be displayed</param>
        /// <param name="region">Region containing the destionations</param>
        /// <returns></returns>
        public string[] destination_buttons(int page,string region)
        {
            if (!m_dictRegionInfo.ContainsKey(region))
                return new string[]{"Back to regions"};
            

            List<String> buttons = new List<string>();
            float num_rows = 2.0f;
            
            //get the number of destinations
            float num_dest = m_dictRegionInfo[region].waypoints.Values.Count;
            
            if( num_dest == 0)
                return new string[] { "Back to regions" };

            //number of buttons per page
            int num_pbuts = (int)( num_dest/((num_rows - 1)*3));
            
            //number of pages
            int num_pages = num_dest % ((num_rows - 1) * 3) == 0 ? num_pbuts : (num_pbuts + 1);

            m_Logger.Info(m_LogMessagePrefix + "number of pages: " + (num_dest % ((num_rows - 1.0f) * 3.0f)));
            m_Logger.Info(m_LogMessagePrefix + "number of dest: " + num_dest);

            if (num_pages > 1)
            {
                if (page == 1)
                {
                    // buttons.Add("-");
                    // buttons.Add("-");
                    //its the first page , add the button [zoneName]:[PAGE]:[nextPageIndex]
                    buttons.Add(region + ":" + "PAGE" + ":" + (page + 1));
                }
                else if (page == num_pages)
                {
                    //its the last page, add the button [zoneName]:[PAGE]:[previousPageIndex]
                    buttons.Add(region + ":" + "PAGE" + ":" + (page - 1));
                    //  buttons.Add("-");
                    //   buttons.Add("-");
                }
                else
                {
                    //add the buttons [zoneName]:[PAGE]:[previousPageIndex] and [zoneName]:[PAGE]:[nextPageIndex]
                    buttons.Add(region + ":" + "PAGE" + ":" + (page + 1));
                    //   buttons.Add("-");
                    buttons.Add(region + ":" + "PAGE" + ":" + (page - 1));
                }
            }

            //add the button "Back to regions"
            buttons.Add("Back to regions");

            //if it is the first page add the button "More info"
            if (page == 1)
                buttons.Add("More info");
         
            //index of the first button/destination
            //TODO possible bug investigate when possible
            int start = ( (int)num_rows - 1 ) * 3 * (page-1);

            //index of the last button/destination
            int stop = Math.Min( start +  ((int)num_rows - 1 ) * 3 ,(int)num_dest ) ;

         
            //get the names of the waypoints
            string [] values = m_dictRegionInfo[region].waypoints.Keys.ToArray();

            //add the buttons [zoneName]:[Destination]
            for (int i = start; i < stop; i++ )
            {
                string label = region + ":" + m_dictRegionInfo[region].waypoints[values[i]].Name;
                buttons.Add( label );
            }
          
            return buttons.ToArray();

        }


        /// <summary>
        /// Builds the menu containing the regions
        /// </summary>
        /// <param name="page">Page of destinations to be displayed</param>
        /// <returns></returns>
        public string[] region_buttons(int page)
        {

            List<String> buttons = new List<string>();
            float num_rows = 2.0f;
            float num_dest = m_dictRegionInfo.Values.Count;

            if (num_dest == 0)
                return new string[0];

            //number of regions
            int num_pbuts = (int)(num_dest / ((num_rows - 1) * 3));

            //number of pages
            int num_pages = num_dest % ((num_rows - 1) * 3) == 0 ? num_pbuts : (num_pbuts + 1);

            m_Logger.Info(m_LogMessagePrefix + "number of pages: " + (num_dest % ((num_rows - 1.0f) * 3.0f)));
            m_Logger.Info(m_LogMessagePrefix + "number of dest: " + num_dest);

            if (num_pages > 1)
            {
                if (page == 1)
                {
                    // buttons.Add("-");
                    // buttons.Add("-");
                    //its the first page , add the button [PAGE]:[nextPageIndex]
                    buttons.Add("PAGE" + ":" + (page + 1));
                }
                else if (page == num_pages)
                {
                    //its the last page, add the button [PAGE]:[previousPageIndex]
                    buttons.Add("PAGE" + ":" + (page - 1));
                    //  buttons.Add("-");
                    //   buttons.Add("-");
                }
                else
                {
                    //add the buttons PAGE]:[previousPageIndex] and [PAGE]:[nextPageIndex]
                    buttons.Add("PAGE" + ":" + (page + 1));
                    //   buttons.Add("-");
                    buttons.Add("PAGE" + ":" + (page - 1));
                }
            }

            //if it is the first page add the button "More info"
            if (page == 1)
                buttons.Add("More info");

            //index of the first button/destination
            //TODO possible bug investigate when possible
            int start = ((int)num_rows - 1) * 3 * (page - 1);

            //index of the last button/destination
            int stop = Math.Min(start + ((int)num_rows - 1) * 3, (int)num_dest);

            //get the names of the zones/regions
            string[] values = m_dictRegionInfo.Keys.ToArray();

            //add the buttons [zoneName]
            for (int i = start; i < stop; i++)
            {
                string label = m_dictRegionInfo[values[i]].Name;
                buttons.Add(label);
            }

            return buttons.ToArray();

        }

        /// <summary>
        /// Displayes a message to the user in the virtual world
        /// The method calls the implementation of llDialog. For more info check the description of llDialog from
        /// secondlife wiki
        /// </summary>
        /// <param name="message"></param>
        /// <param name="buttons"></param>
        /// <param name="k"></param>
        public void SendDialogToUser(string message, string[] buttons, string k)
        {
                   

            // nume_regiune|id obiect|nume_avatar
            string[] dialog_params = k.Split(new char[] { '|' }, StringSplitOptions.None);

            UUID objID = new UUID();
            UUID.TryParse(dialog_params[1], out objID);

            SceneObjectGroup sog = m_dictScenes[dialog_params[0]].GetSceneObjectGroup(objID);

            ScenePresence sp;
            m_dictScenes[dialog_params[0]].TryGetAvatarByName(dialog_params[2],out sp);

            m_Logger.Info("avatar detected : " + sp.Name);

            UUID avatarID = sp.UUID;

            IDialogModule dm = m_dictScenes[dialog_params[0]].RequestModuleInterface<IDialogModule>();


            dm.SendDialogToUser(avatarID, sog.Name, sog.UUID, sog.OwnerID, message, new UUID("00000000-0000-2222-3333-100000001000"), -200, buttons);
                
        }

        /// <summary>
        /// Function that is called when a command is given from the console
        /// The commands which can be written in the console are :
        ///   1. pathf reload -> clear existing data and read the data from the database
        ///   2. pathf conn -> connect to the database
        ///   3. pathf disconn -> disconnect from the database
        /// </summary>
        /// <param name="module">The name of the module</param>
        /// <param name="cmd">The command and it's parameters</param>
        /// 
        public void ProcessConsoleCommand(string module, string[] cmd)
        {
            //if the module has been closed return
            if (!m_Running)
                return;

            //create message for log
            string message = "";
            foreach (string str in cmd)
                message += str + " ";

            m_Logger.Info(m_LogMessagePrefix + "Received command from console: " + message);

            //deals with the command pathf reload
            //the implementation of UpdateNodes() is buggy 
            //it should look like the instructions which deal with the command from the console : 
            //"pathf db reload"
            if (cmd[1] == "reload")
            {
                UpdateNodes();
            }
           
            if (cmd.Length == 2 && cmd[1] == "test1")
            {
                ScenePresence sp;
                m_dictScenes["ACS1"].TryGetAvatarByName("u 1", out sp);
                UUID id = sp.UUID;
                m_Pathfinder.paths.Remove(id);
                m_Pathfinder.paths.Add(id, new List<int>() { 0, 1, 2, 3 });
            }
            if (cmd.Length == 2 && cmd[1] == "test2")
            {
                m_Logger.Info(m_LogMessagePrefix + "computing path");

                ScenePresence sp;
                m_dictScenes["ACS1"].TryGetAvatarByName("u 1", out sp);
                UUID id = sp.UUID;
                m_Pathfinder.paths.Remove(id);
                
                List<int> path = m_Pathfinder.GetPath(id, 0, 2);
                m_Logger.Info(m_LogMessagePrefix + "path computed");
                string msg = "";
                foreach (int i in path) msg += i + "";
                m_Logger.Info(m_LogMessagePrefix + "Path between: 0 and 3 found: " + msg);

                sp.ControllingClient.SendScriptTeleportRequest("primitive", "ACS1", m_Pathfinder.GetCurrentWaypoint(sp.UUID).Pos, new Vector3(0, 0, 0));
                
          
            }
            if(cmd.Length == 2 && cmd[1] == "test3") 
            {
                IDialogModule dm =  m_dictScenes["ACS1"].RequestModuleInterface<IDialogModule>();
                ScenePresence avatar;
                UUID id = new UUID();
                SceneObjectGroup sog = m_dictScenes["ACS1"].GetSceneObjectGroup("TestPan");

                UUID.TryParse("073a7bd7-b151-414f-b77d-04d2d0cd4897",out id);
                if (dm == null)
                   return;

                

                if (!m_dictScenes["ACS1"].TryGetAvatarByName("u 1", out avatar))
                {
                    return;
                }

                string[] buts = new string[3];

                buts[0] = "but 1";
                buts[1] = "but 2";
                buts[2] = "but 3";

                dm.SendDialogToUser(avatar.UUID, "TestPan", sog.UUID, sog.OwnerID, "test dialog", new UUID("00000000-0000-2222-3333-100000001000"), 210, buts);

               
            }
            //deals with the command "pathf db reload"
            if (cmd.Length == 3 && cmd[1] == "db" && cmd[2] == "reload")
            {
                m_Logger.Info(m_LogMessagePrefix + "Reloading data from database");
                //check if there is a connection with the database
                if (!m_DbInterface.connected)
                {
                    m_DbInterface.Connect();
                    if (!m_DbInterface.connected)
                        return;
                }


                m_Logger.Info(m_LogMessagePrefix + "Purging existing data");

                //clear the paths in progress
                //the graph will be changed and the paths might be corrupted by the change
                m_Pathfinder.paths.Clear();

                //read nodes in a buffer
                m_DbInterface.ReadNodes();


                //clear the graph of waypoints
                if (m_WaypointGraph != null)
                    m_WaypointGraph.Clear();


                //TODO uhm i think this is useless since the list is cleared
                //m_WaypointGraph = new List<Node>();

                m_Logger.Info(m_LogMessagePrefix + "Done");

                
                m_Logger.Info(m_LogMessagePrefix + "Retrieving destinations");
                m_dictRegionInfo = m_DbInterface.GetRegionInfo();
                m_Logger.Info(m_LogMessagePrefix + "Done");

                m_Logger.Info(m_LogMessagePrefix + "Retrieving waypoints");

                //fill the graph with empty nodes
                int size = m_DbInterface.NodesNo();
                for (int i = 0; i < size; i++)
                    m_WaypointGraph.Add(null);

                //copy the nodes from the database
                foreach(Scene scene in m_dictScenes.Values)
                    AddNodes(m_DbInterface.GetNodes(scene.RegionInfo.RegionName), scene);

                //update the graph from the Pathfinder instance
                m_Pathfinder.graph = m_WaypointGraph;

                m_Logger.Info(m_LogMessagePrefix + "Done");


                m_Logger.Info(m_LogMessagePrefix + "Loaded waypoint graph with "+ m_DbInterface.NodesNo() + " nodes");
                                
            }

            //deals with the command "pathf db conn"
            if (cmd.Length == 3 && cmd[1] == "db" && cmd[2] == "conn")
            {
                m_DbInterface.Connect();
            }

            //deals with the command "pathf db disconn"
            if (cmd.Length == 3 && cmd[1] == "db" && cmd[2] == "disconn")
            {
                m_DbInterface.Disconnect();
            }
            /*
            if (cmd.Length == 3 && cmd[1] == "db" && cmd[2] == "test")
            {
                string[] tokens = new string[]{
                    "data",
                    "0",
                    "96.5497436523438",
                    "232.178771972656",
                    "33.8567695617676",
                    "1"};


                SendNodeInfo(tokens);
            }
           */
         }
              
    }
}
