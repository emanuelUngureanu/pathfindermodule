
1.Intro
2.Initializare modul
3.Known Bugs


1.Intro

    Modulul pathfinder este o extensie pentru OpenSim si are functionalitatile urmatoare:
        * ghideaza un avatar pana la un punct de interes. De exemplu: intrare sau o facultate
        * permite adaugarea de noi puncte de interes, waypointuri si panouri de comanda
        * foloseste o baza de date pentru stocarea informatiilor despre punctele de interes si waypointuri
        * ruta este calculata folosind algoritmul A*
		
	In OpenSim lumea virtuala este contruita din obiecte puse din editor. Pentru a realiza pathfinding asa 
	cum trebuie este nevoie de informatii despre diverse obiecte ( drumuri,cladiri etc..). In momentul in 
	care am primit sarcina de a realiza o extensie pentru orientare si localizare mare parte din lumea 
	virtuala era deja construita. In afara de asta datorita lipsei de documentatie, timpului mare de cautare
	in codul sursa si a faptului ca nu eram sigur daca o sa gasesc informatiile necesare in codul sursa am
	decis sa implementez un sistem rudimentar de orientare bazat pe waypointuri puse manual in lumea virtuala.
	
	Ideea sistemului de orientare este urmatoarea : 
	In lumea virtuala vor fi puse o serie de waypointuri. Aceste waypointuri se afla la fiecare punct de 
	interes ,intersectie de drumuri si in curbe astfel incat intre doua waypointuri trebuie sa existe cale 
	directa pe un drum din lumea virtuala. Avatarul va fi mereu ghidat sa ajunga la un waypoint, adica 
	va vedea pe ecran o sageata spre un waypoint.
	
	Orice punct de interes/posibila destinatie va avea asociat un waypoint. Iar utilizatorul trebuie ghidat
	spre acel waypoint. Nu este de ajuns ca utilizatorul sa vada pe ecran o sageata care indica destinatia
	deoarece este posibil sa existe obstacole intre el si destinatie. Utilizatorul trebuie sa urmeze o 
	cale pana la destinatie. Acea cale este formata dintr-o serie de waypointuri. Utilizatorul va vedea 
	pe ecran o sageata pana la primul waypoint, in momentul in care ajunge la acesta va vedea o sageata pana
	la urmatorul waypoint din cale si tot asa pana cand ajunge la destinatie.
	
	Waypointurile pot fi vazute si sub forma de graf. Si problema de orientare in lumea virtuala poate fi redusa
	la o problema de gasire a unei cai minime intr-un graf , pentru rezolvarea careia am ales algoritmul A*.
	
	In continuare o sa prezint implementarea.
	
	Waypointurile sunt puncte/locatii din lumea virtuala rezulta ca ele vor fi caracterizate de pozitia in lume,
	adica de coordonatele x,y,z.
	Pentru a folosi A* este necesara reprezentarea waypointurilor ( pe care o sa le numesc si noduri) sub forma
	de graf iar graful va fi reprezentat in memorie sub forma unei liste de adiacenta. Prin urmare nodul va fi
	caracterizat si prin index in lista de adiacenta, si indicii nodurilor vecine. De asemenea nodurile vor fi
	stocate intr-o baza de date.
	
	Waypointurile/Nodurile sunt caracterizate prin : POZITIA xyz , INDEX-ul in lista de adiacenta si INDICII  
	nodurilor vecine.

    Nodurile pot fi si destinatii/puncte de interes deci trebuie sa aibe un NUME si o DESCRIERE. Destinatiile
	vor fi afisate intr-un meniu in lumea virtuala. In butoanele meniului nu pot fi scrise multe caractere asa 
	ca este nevoie de un nume scurt/prescurtare a destinatiei. Totusi trebuie sa poata fi afisat tot numele 
	destinatiei si eventual o descriere care vor fi stocate in DESCRIERE si afisate intr-o fereastra de dialog 
	in lumea virtuala.
	
	Ar fi destul de neplacut ca utilizatorul sa vada toate destinatiile posibile din meniu, mai ales ca sunt multe 
	destinatii in Politehnica. O solutie ar fi sa grupam aceste destinatii in functie de scop sau regiunea in care
	se afla. De exemplu poate exista grupul intrarilor in campus, grupul facultatilor din poli sau al salilor dintr-o
	facultate. 
	
	In momentul in care utilizatorul deschide meniul cu destinatii va vedea serie de zone/regiuni care contin mai 
	multe puncte de interes : Intrari, ACS, Campus, etc. , apoi cand o sa selecteze o zona va putea vedea toate 
	punctele de interes din zona respectiva. Pentru reprezentarea zonelor este nevoie de o noua structura de date 
	"Regiune" caracterizata prin INDEX, NAME, DESCRIPTION si FULL_NAME. Index este necesar deoarece regiunile vor 
	fi retinute intr-o baza de date si trebuie identificate in mod unic, iar campurile name, description si 
	full_name sunt necesare deoarece numele zonelor va fi scris in butoanele unui meniu si trebuie sa fie scurte, 
	descrierea trebuie sa poata fi afisata separat, iar full_name este necesar deoarece numele facultatilor va fi 
	scris prescurtat si trebuie sa existe si varianta intreaga.
	
	Zonele/Regiunile vor fi caracterizate prin : Id, Name, Description, full_name.
	
	Fiecare regiune contine o multime de puncte de interes, trebuie sa identificam care sunt punctele dintr-o zona 
	de interes ,prin urmare trebuie adaugat campul REG de tipul int in structura de date Node care va reprezenta
	campul Index al unei Regini. 
	
	Nodurile sunt caracterizate prin :   INDEX-ul in lista de adiacenta, POZITIA xyz , REG , regName, DESCRIERE, NAME si 
	INDICII nodurilor vecine. 
	
	Pasii urmati de modul pentru a rula sunt :
	
	1. Initializare
	2. Daca simulatorul se inchide sari la 5 altfel asteapta comanda de la un script.
	3. Proceseaza comanda primita la pasul 2
	4. Sunt apelate metodele RemoveRegion si Close.
	
	Pentru mai multe informatii despre functionarea modulului vedeti codul sursa mai ales metodele de tratare a 
	comenzilor de la scripturi. ( cele care incep cu "Process")
	
	
	

2.Initializare modul

    Pasii urmati de modul pentur initializare sunt determinati de metodele interfetei ISharedRegionModule.
    Metodele sunt :
        a)Initialise(IConfigSource config)
        b)AddRegion(Scene scene)
        c)RegionLoaded(Scene scene)
        d)PostInitialise()
        d)RemoveRegion(Scene scene)
        e)Close()
        
        
    Fiecare metoda este apelata de catre OpenSim in ordinea in care sunt enumerate, cu exceptia AddRegion,
    RegionLoaded si RemoveRegion care pot fi apelate in momenul in care se adauga/scoate o regiune din 
    OpenSim iar aplicatia nu este stadiul de pornire sau oprire.

    Descriere functionalitati metode:
    
        a)Initialise(IConfigSource config)
            Metoda este apelata cand modulul este incarcat in OpenSim, dar nici o regiune nu a fost adaugata
            inca. Actiunile realizate de metoda sunt :
                * initializare interfata cu baza de date
                * citire informatii din baza de date intr-un buffer 
                * creere instanta Pathfinder
                * initializare interfata cu consola si inregistrare comanda
                
        b)AddRegion(Scene scene)
            Metoda este apelata cand adaugata o regiune in OpenSim, regiunea nu este incarcata inca.
            Metoda nu face nimic daca aplicatia este in stadiul de initializare.
            In cazul in care o regiune este adaugata in OpenSim exista posibilitatea ca regiunea sa fi fost 
            scoasa inainte si nodurile dezactivate, astfel ele trebuie reactivate.
                
        c)RegionLoaded(Scene scene)
            Metoda este apelata cand o regiune este incarcata ( initializata) in OpenSim.
            Actiunile realizate de metoda sunt:
                * adaugare interfata cu scripturi specifica scenei
                * inregistrare metoda care va trata mesajele de la scripturi
                * adaugare scena in dictionar
                * la prima apelare a metodei este initializat graful cu noduri goale, valori null deoarece 
                  nodurile dintr-o regiune sunt adaugate cand regionea in care se afla este incarcata
                  iar ordinea in care sunt adaugate regiunile nu este identica cu ordinea in care sunt stocate
                * seteaza nodurile corespunzatoare acestei scene in graph
                * daca graful nu este gol actualizeaza graful din instanta de tip Pathfinder. Cand am scris codul
                  nu eram sigur daca pastreaza referinta sau copiaza informatia asa ca trebuia sa fiu sigur
                  ca graful din instanta Pathfinder cel care trebuie
                  
        d)PostInitialise()
            Metoda nu face nimic
            
        d)RemoveRegion(Scene scene)
            Metoda este apelata cand o regiune este scoasa din modul. Situatiile in care ea poate fi apelata sunt:
            eliminare manuala de regiune, simulatorul este in starea de inchidere si (poate, nu sunt sigur) modulul
            este descarcat/scos manual din OpenSim.
            Actiunele realiate de metoda sunt :
                * nodurile care sunt regiunea eliminta sunt marcate inactive
                * este eliminata interfata cu scripturile
                * este eliminata scena din dictionar
                * daca nu mai exista scene in simulator este inchisa conexiunea cu baza de date
                
        e)Close()
            Metoda este apelata in aceleasi situatii ca si RemoveRegion.
            Actiunile realizate de metoda sunt :
                * modulul este marcat ca descarcat ( variabila running = false)
                * dictionarele sunt golite de date
                * datele locale din "interfete" sunt sterse si interfetele sunt inlaturate
                
                
                
 3. Known Bugs
 
    1.  Posibil bug la trimiterea vecinilor unui nod in baza de date. Inainte sa se trimita efectiv vecinii trebuie
         stersi din baza de date vecinii care existau inainte
         
    2.  Mutexul incetineste foarte mult modulul. Uneori trece foarte mult timp pana cand modulul proceseaza un mesaj
         primit de la un script. Trebuie gasita o cale de a serializa modificarea datelor globale printr-un mecanism de sincronizare
         sau prin structuri de date thread-safe.
         
    3.  Cand scriptul Commander trimite comanda "Send data" in consola este afisata exceptia ca ar exista un reader deschis in in 
         interfata cu baza de date. Dupa ce apare aceasta exceptie trebuie repornit modulul deoarece comunicatiile cu baza de date
         sunt afectate de acel bug.
         
    4.  Panourile asculta mesaje pe acelasi canal. Ele nu trebuie sa asculte pe acelasi canal deoarece in momentul in care un utilizator
         alege o destinatie alte panouri apropriate vor primi mesajul de alegere al destinatiei si vor trimite si ele comanda de alegere
         a destinatiei catre modul. Rezultatul fiind o serie de comenzi de alegere a destinatiei trimise catre modul, fiecare avand setat
         ca punct de start punctul asociat panoului.  O posibila solutie este setarea canalului "channel" la valoarea -(integer)llGetObjectDesc().
         
         
     5. Alt bug este la marcarea vizuala a unei destinatii. Parca destinatia ar fi formata din valori intregi si nu float. Solutie : Waypointuri destul
         de mari astfel incat sa includa marcajele vizuale.
         
         
     6. Regiunile/Grupurile de noduri trebuie sa contina si pagina "Descriere" care sa contina descrierea regiunii curente. Butonul pentru descrierea
         grupului trebuie sa fie pe prima pagina cu destinatii 
         
     7. In momentul in care apare o eroare in comunicarea  cu baza de date interfata cu baza de date/ clasa care intermediaza aceasta comunicare
         trebuie sa marcheze ca nu exista conexiune cu baza de date.
         
      8. Cand se comunica cu baza de date trebuie sa se verifice daca exista o conexiune cu aceasta si daca nu exista sa se incerce realizarea acesteia.
         




    