//Script used by the developers to manipulate the waypoints,
//send data and reload data from the database

integer channel = -210;
default
{
    state_entry()
    {
        llSay(0, "Script running");
        llListen(channel,"", "","");
    }
    touch_start(integer num_detected)
    {
         //  modSendCommand("PathfinderModule", "" , "reload");
        //   llSay(0,"send reload command to module");
           
           llDialog(llDetectedKey(0), "Options:",
                 ["Send data","Reload data", "Set phantom","Unset phantom","Hide", "Show"], channel);
           
    }
    
    listen(integer chan, string name, key id, string mes)
    {
       if(mes == "Reload data")
                modSendCommand("PathfinderModule", "" , "reload");
                     
    }
}