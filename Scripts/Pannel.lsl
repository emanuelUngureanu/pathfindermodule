//Compact function to put buttons in "correct" human-readable order
//
//Script used to in the process of displaying a menu to the avatar
//When an avatar touches the object containing this script it will announce
//the module that an avatar wants to see the menu and the module
//will display the menu to the avatar
//
//When the avatar choses an option from the menu
//the script will send the option the module
//and module will process and message and chose an apropriate
//response

integer channel=-200;

//A pannel depends on a waypoint:
//When a path is requested the module uses as starting point
//a waypoint. The waypoint used is identified by  its index
//The index of the waypoint is set in the description field of the prim
//containing this script
 
default
{
    state_entry()
    {  
 
    llListen(channel,"", "","");
    }
 
    touch_start(integer total_number)
    {
      key id = llDetectedKey(0);
      string k = llGetRegionName()+"|"+llGetKey()+"|"+llKey2Name(id);
      modSendCommand("PathfinderModule", "PAGE:1" , k);
    
    }
 
    listen(integer _chan, string _name, key _id, string _option)
    {
        llSay(0, _name + " chose option " + _option);
        string k = llGetRegionName()+"|"+llGetKey()+"|"+llKey2Name(_id)+"|"+ llGetObjectDesc();
        modSendCommand("PathfinderModule", _option , k);
       
    }
     
}