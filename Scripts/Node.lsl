//Script for a waypoint
//The script has the following functionalities:
//   *  sends data to the module when it receives the message "Send data" 
//       or the prim is touched by an avatar. 
//   *  activates/deactivates the attribute "Phantom" of the prim containing this script
//       when it receives the message "Set phantom"/"Unset phantom"
//   *   makes the object invisible when it receives the message "Hide"
//   *   makes the object visible when it receives the message "Show"
//   *   notifies the module when an avatar collides with the object


// In order to use this script as a waypoint the name of the prim
// containing this script must be the index of the waypoint 
// and the description field of the prim must have the following format
// <Node group name> <neighbour1> <neighbour2> ... <neighbourn>
// the attributes contained in the description field must be separated by space
// As example i will provide the configuration of two nodes used in a local test:
// Prim name: 1
// Prim description: Reg1 2

// Prim name: 2
// Prim description: Reg1 1 3

// Prim name: 3
// Prim description: Reg1 2

integer channel = -210;

default
{
    state_entry()
    {
        llSay(0, "Script running");
       
       llListen(channel,"","","");
    }
    
    listen(integer chanel, string name, key id, string message)
    {
        
        if(message == "Send data")
        {
                vector pos = llGetPos();
                  
            string msg = "data|"+ llGetRegionName() +"|"+ llGetObjectName()+"|";
            
            msg += pos.x + "|" +pos.y + "|"+pos.z;
        
            
          //  message += pos.x + "|" +pos.y + "|"+pos.z + "|";
         //   message += llGetRegionName();
            string desc = llGetObjectDesc();
            if(desc != "")
            {
                msg += "|"+desc;
            }
         
             modSendCommand("PathfinderModule", msg , "");
        }
        if(message == "Set phantom")
             llVolumeDetect(TRUE);
        else if(message == "Unset phantom")
        {
            llSay(0,message);
            
             //llVolumeDetect(FALSE);
             llSetStatus(STATUS_PHANTOM,FALSE);
            }
        else if(message == "Show")
             llSetAlpha(1.0,ALL_SIDES);
        else if(message == "Hide")
              llSetAlpha(0.0,ALL_SIDES);
    }
    
    
     touch_start(integer num_detected)
    {
        vector pos = llGetPos();
              
        string message = "data|"+  llGetRegionName() +"|"+ llGetObjectName()+"|";
        
        message += pos.x + "|" +pos.y + "|"+pos.z;
    
        
      //  message += pos.x + "|" +pos.y + "|"+pos.z + "|";
     //   message += llGetRegionName();
        string desc = llGetObjectDesc();
        if(desc != "")
        {
            message += "|"+desc;
        }
     
         modSendCommand("PathfinderModule", message , "");
    }
    
    
    
    collision_start(integer num_detected)
    {
        integer i = 0;
        string message = "waypoint|"+llGetObjectName()+"|";
        message +=  llGetRegionName()+"|";
        for(; i < num_detected; i++)
        {
            string avatarName = llKey2Name(llDetectedKey(i));
            
            modSendCommand("PathfinderModule", message + avatarName, "");
        }
        llSleep(1);
    }

   
}